<?php


namespace App;

Class Result extends Database
{
    private $id;
    private $name;
    private $roll;
    private $markBangla;
    private $markEnglish;
    private $markMath;
    private $gradeBangla;
    private $gradeEnglish;
    private $gradeMath;


    public function setData($postData){
        if(array_key_exists("id",$postData)){
            $this->id = $postData['id'];
        }
        if(array_key_exists("roll",$postData)){
            $this->roll = $postData['roll'];
        }
        if(array_key_exists("$this->name",$postData)){
            $this->name = $postData['$this->name'];
        }
        if(array_key_exists("$this->markBangla",$postData)){
            $this->markBangla = $postData['$this->markBangla'];
        }
        if(array_key_exists("$this->markEnglish",$postData)){
            $this->markEnglish = $postData['$this->markEnglish'];
        }
        if(array_key_exists("$this->markMath",$postData)){
            $this->markMath = $postData['$this->markMath'];
        }
        if(array_key_exists("$this->gradeBangla",$postData)){
            $this->gradeBangla = $postData['$this->gradeBangla'];
        }
        if(array_key_exists("$this->gradeEnglish",$postData)){
            $this->gradeEnglish = $postData['$this->gradeEnglish'];
        }
        if(array_key_exists("$this->gradeMath",$postData)){
            $this->gradeMath = $postData['$this->gradeMath'];
        }
    }

    public function store(){

        $arrayData = array ($this->name, $this->roll,$this->markBangla, $this->markEnglish, $this->markMath,$this->gradeBangla,$this->gradeEnglish,$this->gradeMath);

        $sql = "Insert into result(name,roll,mark_bangla,mark_english,mark_math,grade_bangla,grade_english,grade_math) VALUE (?,?,?,?,?,?,?,?)";

            $STH = $this->DBH->prepare($sql);

            $success = $STH->execute($arrayData);

            if($success){

                Message::message("Success! Data Has Been Inserted Successfully!<br>");
            }
            else{

                Message::message("Failed! Data Has Not Been Inserted !<br>");
            }
    }

}

